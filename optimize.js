const fs=require('fs');
const child_process=require('child_process');

const removeQuotes=function(str) {
    return str.trim().replace(/"/g, '').replace(/'/g, '');
};

var pathFrom=removeQuotes(process.argv[2]).trim();
var pathTo=removeQuotes(process.argv[3]).trim();

var files=[];
try {
    files=fs.readdirSync(pathFrom);
} catch(e) {
    console.log(e);
    process.exit(1);
}
console.log('Found ' + files.length + ' files');
if(files.length<1) {
    console.log('No files to process');
    process.exit(0);
}
files.reverse();
files.forEach(function(file) {
    var fileFrom=pathFrom + '/' + file;
    var fileTo=pathTo + '/' + file;
    if(file.indexOf('.')!=-1) { //file has string
        if (!fs.existsSync(fileTo)) {
            var ch=child_process.spawn('node', ['worker.js', fileFrom, fileTo]);
        }
    }
    // }else{
    //     console.log('file do not have extension, just copy to destination')
    //   // fs.copyFileSync(fileFrom, fileTo);
    // }
});
//curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
//sudo apt-get install -y nodejs
// to run execute in command line: node optimize.js "/var/www/foo"
