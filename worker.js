const fs=require('fs');
const Jimp=require('jimp');

function optimize(fileFrom, fileTo, overwrite=false)
{
    return Jimp.read(fileFrom, function(err, image) {
        console.log('Jimp read file ' + fileFrom);
        if(err) {
            return Promise.reject('Error reading: ' + err);
        }
        return image.resize(414, Jimp.AUTO) // resize width but keep height auto so we do not deforme image
            .quality(55)                    // set JPEG quality
            .write(fileTo, function(err) {
                if(err) {
                    return Promise.reject('Error writing: ' + err);
                }
                return Promise.resolve();
            });
    });
}

const removeQuotes=function(str) {
    return str.trim().replace(/"/g, '').replace(/'/g, '');
};

var fileFrom=removeQuotes(process.argv[2]).trim();
var fileTo=removeQuotes(process.argv[3]).trim();

if (fs.existsSync(fileTo)) {
    process.exit(1);
} else {
    optimize(fileFrom, fileTo).then(function() {
        process.exit(0);
    }).catch(function(err) {
        console.log(err);
        process.exit(1);
    });
}
